import React from 'react';
import { Link } from 'react-router-dom';

const QuoterFooter = (props) => (
    <div id='footer'>
        <Link className='btn btn-primary' to={`/?quote=${props.startingQuoteId}`}>
            Back to Beginning
        </Link>
    </div>
)

export default QuoterFooter