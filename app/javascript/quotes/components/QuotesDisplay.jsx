import React from 'react'
import { Redirect } from 'react-router-dom'
import queryString from 'query-string'
import axios from 'axios'
import QuoteText from './QuoteText'
import QuoteNavigation from './QuoteNavigation'
import QuoteFooter from './QuoteFooter'

class QuotesDisplay extends React.Component{
    constructor(){
        super()
        this.state = {
            quote: {}
        };
    }

    fetchQuote(id){
        axios.get(`api/quotes/${id}`)
            .then(response => {
                this.setState({ quote: response.data })
            })
            .catch(error => {
                console.error(error)
                this.setState({ fireRedirect: true })
            });
    }

    setqQuoteIdFromQueryString(qs){
        this.qsParams = queryString.parse(qs)
        if (this.qsParams.quote){
            //assigns quote ID from URL string
            this.quoteId = Number(this.qsParams.quote)
        }else{
            this.quoteId = this.props.startingQuoteId
            this.props.history.push(`/?quote=${this.quoteId}`)
        }
    }

    componentDidMount(){
        this.setqQuoteIdFromQueryString(this.props.location.search)
        this.fetchQuote(this.quoteId)
    }

    componentWillReceiveProps(nextProps){
        this.setqQuoteIdFromQueryString(nextProps.location.search)
        this.fetchQuote(this.quoteId)
    }

    render(){
        const quote = this.state.quote
        const nextQuoteId = quote.next_id
        const previousQuoteId = quote.previous_id

        return(
            <div>
                <div className='quote-container'>
                    {this.state.fireRedirect && 
                    <Redirect to={'/'} />
                    }
                    {previousQuoteId && 
                    <QuoteNavigation direction='previous' otherQuoteId={previousQuoteId} />
                    }
                    <QuoteText quote={this.state.quote} />
                    {nextQuoteId && 
                    <QuoteNavigation direction='next' otherQuoteId={nextQuoteId} />
                    }
                </div>
                {this.state.quote.id !== parseInt(this.props.startingQuoteId, 10) &&
                <QuoteFooter startingQuoteId={this.props.startingQuoteId}/>
                }
            </div>
        )
    }
}

export default QuotesDisplay